class Cell(Widget):
    r, g, b, a = NumericProperty(1), NumericProperty(0), NumericProperty(0), NumericProperty(0)


class PongGame(Widget):
    w, h = 200, 200
    life = Life(w, h) # Where I implement GOL poorly
    cells = []

    def start(self):
        for i in range(self.w):
            r = []
            self.cells.append(r)
            for j in range(self.h):
                w = Cell(pos=(i * 5, j * 5))
                self.add_widget(w) # Don't know if this is the right way to add widgets, but I had a lot of problems trying to get it to work, so just went for this dumb double for loop way
                r.append(w)

    def render(self, _):
        self.life.update()
        for i in range(self.w):
            for j in range(self.h):
                # Setting this each time isn't necessary
                # But I don't think it should affect performance
                if self.life.current_state[i][j]:
                    self.cells[i][j].r = 1
                else:
                    if self.life.has_been_alive[i][j]:
                        self.cells[i][j].r = 0.1
                    else:
                        self.cells[i][j].r = 0


class LifeApp(App):
    def build(self):
        game = PongGame()
        game.start()
        Clock.schedule_interval(game.render, 0.1 / 60.0)
        return game


if __name__ == '__main__':
    LifeApp().run()