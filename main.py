import operator

LIBRARIES = []
DAYS_TO_SCAN = 0
TOTAL_BOOKS = 0
SCORE_LIST = []
ALL_BOOKS = {}

PROCESSED_BOOKS = []


class Book:
    def __init__(self, id, score):
        self.id = id
        self.score = score


def load_file(full_path: str):
    input_file = open(full_path, 'r')

    first_line = input_file.readline()
    items = first_line.split(' ')

    TOTAL_BOOKS = int(items[0])
    TOTAL_LIBRARIES = int(items[1])
    DAYS_TO_SCAN = int(items[2])

    second_line = input_file.readline()

    items = second_line.split(' ')

    for index, book_score in enumerate(items):
        ALL_BOOKS[index] = int(book_score)

    for index, line in enumerate(input_file):
        if len(line) <= 1:
            continue
        library_information = line.split(' ')
        lib = Lib(index)
        book_number = int(library_information[0])
        lib.sign_up_days = int(library_information[1])
        lib.shipping_amount = int(library_information[2])

        books_information = input_file.readline().split(' ')

        for book_id in books_information:
            lib.books.append(Book(int(book_id), ALL_BOOKS[int(book_id)]))

        LIBRARIES.append(lib)


def ouput_file(file_name):
    output_file = open(file_name, 'w')
    output_file.write(f"{len(LIBRARIES)}\n")
    for lib in LIBRARIES:
        output_file.write(f"{lib.id} {len(lib.books)}\n")
        for book in lib.books:
            # WILL CAUSE A TRAILING SPACE MAY BE PROBLEM DONT FORGET
            output_file.write(f"{book.id} ")
        output_file.write('\n')


class Lib:
    def __init__(self, id):
        self.id = id
        self.weight = 0
        self.books = []
        self.sign_up_days = 0
        self.shipping_amount = 0
    
    def value(self):
        remove_processed = list(set(self.books) - set(PROCESSED_BOOKS))
        books_without_duplicates = list(dict.fromkeys(remove_processed))

        number_of_unique_books = len(remove_processed) - len(remove_processed)
        value = self.shipping_amount - self.sign_up_days + number_of_unique_books
        print(f"Value {value}")
        return value


def algo():
    global LIBRARIES
    ORDER = []

    while len(ORDER) < len(LIBRARIES):
        best_lib = None
        best_lib_value = -1000000
        
        for lib in LIBRARIES:
            # may want to remove duplicates
            lib.books.sort(key=operator.attrgetter('score'), reverse=True)
            v = lib.value()
            if v > best_lib_value:
                best_lib_value = v
                best_lib = lib

        for b in best_lib.books:
            PROCESSED_BOOKS.append(b)
        ORDER.append(best_lib)
    LIBRARIES = ORDER

if __name__ == "__main__":

    files = [
        # "a_example",
        "b_read_on",
        # "c_incunabula",
        # "d_tough_choices",
        # "e_so_many_books",
        # "f_libraries_of_the_world",
    ]
    for input_file in files:
        print(f"Processing: inputs/{input_file}.txt")
        load_file(f"inputs/{input_file}.txt")
        algo()
        ouput_file(f"outputs/{input_file}_output.txt")
        LIBRARIES = []
        DAYS_TO_SCAN = 0
        TOTAL_BOOKS = 0
        SCORE_LIST = []
        ALL_BOOKS = {}
